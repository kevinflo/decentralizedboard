import * as types from "./action-types";
import board from "../ethereum/board";
import web3 from "../ethereum/web3";

const boardAttrs = new Map([
  ["platform", "address"],
  ["allowPosterEditing", "bool"],
  ["allowOwnerModeration", "bool"],
  ["allowBoardTipping", "bool"],
  ["allowTaxation", "bool"],
  ["allowPostTipping", "bool"],
  ["allowComments", "bool"],
  ["allowCommentTipping", "bool"],
  ["readOnly", "bool"],
  ["fee", "uint"],
  ["url", "bytes32"],
  ["name", "bytes32"],
  ["totalPaidTax", "uint"],
  ["owner", "address"]
]);

let constructBoardData = (address, boardValues) => {
  let boardData = { address };
  const attributes = [...boardAttrs.keys()];
  for (let i = 0; i < attributes.length; i++) {
    if (boardAttrs.get(attributes[i]) === "bytes32") {
      boardData[attributes[i] + "Str"] = web3.utils
        .toAscii(boardValues[i])
        .replace(/\u0000/g, "");
    }

    boardData[attributes[i]] = boardValues[i];
  }
  return boardData;
};

export function loadBoard(address) {
  return async function(dispatch, getState) {
    dispatch(boardRequest(address));
    let boardPromises = [];

    for (let attribute of boardAttrs.keys()) {
      boardPromises.push(board.methods[attribute].call().call());
    }

    let boardValues = await Promise.all(boardPromises);

    let boardData = constructBoardData(address, boardValues);

    dispatch(boardSuccess(address, boardData));
  };
}

export function loadBoardExpanded(address) {
  return async function(dispatch, getState) {
    dispatch(boardRequest(address));
    // const name = await board.methods.name().call();
    let boardPromises = [];

    for (let attribute of boardAttrs.keys()) {
      boardPromises.push(board.methods[attribute].call().call());
    }

    const getPostsLengthPromise = board.methods.getPostsLength().call();

    let boardValues = await Promise.all(
      [boardPromises, [getPostsLengthPromise]].map(innerPromiseArray => {
        return Promise.all(innerPromiseArray);
      })
    );

    let boardData = constructBoardData(address, boardValues[0]);
    boardData.postsLength = boardValues[1];

    dispatch(boardSuccess(address, boardData));
  };
}

export function boardSetUrl(url) {
  return async function(dispatch, getState) {
    let address = getState().eth.accounts[0];
    dispatch(boardSetUrlRequest(url));
    let urlBytes32 = web3.utils.fromAscii(url);
    try {
      let gasCost = await board.methods
        .updateUrl(urlBytes32)
        .send({ from: address });

      await board.methods
        .updateUrl(urlBytes32)
        .send({ from: address, gas: gasCost }); // maybe assign and do something when done

      dispatch(boardSetUrlSuccess(urlBytes32, url));
    } catch (err) {
      dispatch(boardSetUrlFailure(err));
    }
  };
}

export function loadPostsLength(address) {
  return async function(dispatch, getState) {
    dispatch(boardPostsLengthRequest(address));

    const postsLength = await board.methods.getPostsLength().call();
    dispatch(boardPostsLengthSuccess(address, postsLength));
  };
}

const boardPostsLengthRequest = address => {
  return {
    type: types.BOARD_POSTS_LENGTH_REQUEST
  };
};

const boardPostsLengthSuccess = (address, postsLength) => {
  return {
    type: types.BOARD_POSTS_LENGTH_SUCCESS,
    postsLength
  };
};

const boardRequest = address => {
  return {
    type: types.BOARD_REQUEST,
    address
  };
};

const boardSuccess = (address, boardData) => {
  return {
    type: types.BOARD_SUCCESS,
    boardData
  };
};

const boardSetUrlRequest = () => {
  return {
    type: types.BOARD_SET_URL_REQUEST
  };
};

const boardSetUrlSuccess = (url, urlStr) => {
  return {
    type: types.BOARD_SET_URL_SUCCESS,
    payload: { url, urlStr }
  };
};
const boardSetUrlFailure = err => {
  return {
    type: types.BOARD_SET_URL_FAILURE
  };
};
