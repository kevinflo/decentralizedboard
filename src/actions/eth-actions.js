import * as types from "./action-types";
import web3 from "../ethereum/web3";

export function loadAccounts() {
  return async function(dispatch, getState) {
    dispatch(accountsRequest());
    const accounts = await web3.eth.getAccounts();
    if (accounts && accounts.length) {
      dispatch(accountsSuccess(accounts));
    } else {
      dispatch(accountsFailure());
    }
  };
}

const accountsRequest = () => {
  return {
    type: types.ACCOUNTS_REQUEST
  };
};

const accountsSuccess = accounts => {
  return {
    type: types.ACCOUNTS_SUCCESS,
    accounts
  };
};

const accountsFailure = accounts => {
  return {
    type: types.ACCOUNTS_FAILURE,
    accounts
  };
};
