import * as types from "./action-types";
import board from "../ethereum/board";
import dposts from "../ethereum/dposts";
import web3 from "../ethereum/web3";
import normalizeUrl from "normalize-url";
import uuidv4 from "uuid/v4";

const postAttrs = new Map([
  ["index", "uint"],
  ["poster", "address"],
  ["title", "bytes32"],
  ["body", "string"],
  ["link", "bytes32"],
  ["boardTip", "uint"],
  ["tax", "uint"],
  ["blockNumber", "uint"],
  ["createdAt", "uint"],
  ["visible", "bool"],
  ["hiddenByOwner", "bool"],
  ["tipTotal", "uint"]
]);

/* 
  should switch that to object values with more info 
  like solidity type, url or not, etc, derived or created, etc
  then conditionally handle based on that later instead of
  special casing
*/

const postCreateAttrs = ["title", "body", "link", "boardTip", "taxSubmitted"];

let normalizePosts = posts => {
  return posts.map(post => {
    let normalizedPost = {};
    for (let attr of postAttrs.keys()) {
      if (
        typeof post[attr] === "string" &&
        post[attr].length &&
        postAttrs.get(attr) === "bytes32"
      ) {
        let stringVersion = web3.utils
          .toAscii(post[attr])
          .replace(/\u0000/g, "");
        let normalizedStringVersion = "";

        if (attr === "link") {
          // blegh
          if (stringVersion.length) {
            normalizedStringVersion = normalizeUrl(stringVersion);
          }
          normalizedPost[attr + "StrNormalized"] = normalizedStringVersion;
        }
        normalizedPost[attr + "Str"] = stringVersion;
      }

      normalizedPost[attr] = post[attr];
    }
    return normalizedPost;
  });
};

export function loadPosts(force) {
  return async function(dispatch, getState) {
    if (getState().posts && getState().posts.length && !force) {
      return;
    }

    dispatch(getPostsRequest());

    const postsLength = await board.methods.getPostsLength().call();
    let getPostPromises = [];

    for (let i = 0; i < postsLength; i++) {
      getPostPromises.push(board.methods.posts(i).call());
    }

    const posts = await Promise.all(getPostPromises);
    const normalizedPosts = normalizePosts(posts);

    dispatch(getPostsSuccess(normalizedPosts));
  };
}

export function toggleVisibility(postIndex) {
  return async function(dispatch, getState) {
    let post = getState().posts[postIndex];
    if (typeof post !== "object") {
      return;
    }
    let visibility = !post.visible;
    dispatch(setPostVisibilityRequest(visibility));
    let address = getState().eth.accounts[0];
    try {
      let gasCost = await board.methods
        .editPostVisibility(postIndex, visibility)
        .estimateGas({ from: address });

      await board.methods
        .editPostVisibility(postIndex, visibility)
        .send({ from: address, gas: gasCost });

      dispatch(setPostVisibilitySuccess(visibility));
    } catch (err) {
      dispatch(setPostVisibilityFailure(err));
    }
  };
}

export function createPost(post) {
  return async function(dispatch, getState) {
    let postCreateArgs = [];
    let address = getState().eth.accounts[0];
    for (let attr of postCreateAttrs) {
      if (postAttrs.get(attr) === "bytes32") {
        postCreateArgs.push(web3.utils.fromAscii(post[attr]));
      } else {
        postCreateArgs.push(post[attr]);
      }
    }
    let pendingId = uuidv4();

    let postValue = 0;
    if (typeof postCreateArgs[3] === "undefined") {
      postCreateArgs[3] = 0;
    } else if (postCreateArgs[3]) {
      postCreateArgs[3] = parseFloat(postCreateArgs[3]) * 1000000000000000000;
      postValue += postCreateArgs[3];
    }

    let currentTax;

    if (typeof postCreateArgs[4] === "undefined") {
      postCreateArgs[4] = 0;
    } else if (postCreateArgs[4]) {
      currentTax = await dposts.methods.determineTax().call();
      postValue += parseInt(currentTax, 10);
      postCreateArgs[4] = parseInt(currentTax, 10);
    }

    dispatch(postRequest(postCreateArgs, pendingId));

    try {
      let gasCost = await board.methods
        .createPost(...postCreateArgs)
        .estimateGas({
          from: address,
          value: postValue
        });

      await board.methods.createPost(...postCreateArgs).send({
        from: address,
        gas: gasCost,
        value: postValue
      });

      dispatch(postSuccess(postCreateArgs, pendingId));
      dispatch(loadPosts(true));
    } catch (err) {
      console.error(err);
      dispatch(postFailure(postCreateArgs, pendingId));
    }
  };
}

const postRequest = (post, pendingId) => {
  return {
    type: types.POST_REQUEST,
    payload: { post, pendingId }
  };
};

const postSuccess = (post, pendingId) => {
  return {
    type: types.POST_SUCCESS,
    payload: { post, pendingId }
  };
};

const postFailure = (post, pendingId) => {
  return {
    type: types.POST_FAILURE,
    payload: { post, pendingId }
  };
};

const getPostsSuccess = posts => {
  return {
    type: types.GET_POSTS_SUCCESS,
    posts
  };
};

const getPostsRequest = posts => {
  return {
    type: types.GET_POSTS_REQUEST,
    posts
  };
};

const setPostVisibilityRequest = visibility => {
  return {
    type: types.SET_POST_VISIBILITY_REQUEST,
    visibility
  };
};

const setPostVisibilitySuccess = visibility => {
  return {
    type: types.SET_POST_VISIBILITY_REQUEST,
    visibility
  };
};

const setPostVisibilityFailure = err => {
  return {
    type: types.SET_POST_VISIBILITY_FAILURE,
    err
  };
};
