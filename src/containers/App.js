import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  loadBoard,
  loadBoardExpanded,
  loadPostsLength
} from "../actions/board-actions";
import { loadAccounts } from "../actions/eth-actions";
import { BOARD } from "../addresses";
import { connect } from "react-redux";
import MainNavbar from "../components/MainNavbar";
import Byline from "../components/Byline";
import web3 from "../ethereum/web3";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: "center",
    color: theme.palette.text.primary
  },
  link: {
    color: "rgb(225, 0, 80)",
    textDecoration: "none",
    "&:hover": {
      textDecoration: "underline"
    }
  },
  hr: {
    width: "50%",
    backgroundColor: "#fff"
  },
  divider: {
    width: "50%",
    margin: "12px auto"
  }
});

class App extends Component {
  state = { doop: null };

  metamaskStatusPoll = setInterval(() => {
    console.log("mmstatus");
    if (!this.props.accounts[0] && this.props.notLoggedIntoMetamask) {
      this.props.loadAccounts();
    }
  }, 5000);

  componentDidMount() {
    if (!web3) {
      return;
    }
    this.props.loadBoardExpanded(BOARD);
    this.props.loadAccounts();
  }

  render() {
    if (this.props.accounts[0]) {
      clearInterval(this.metamaskStatusPoll);
    }

    if (!web3) {
      return <div />;
    }
    const { classes, accounts } = this.props;

    return (
      <div className={classes.root}>
        <MainNavbar />
        <Byline />
        <div className={classes.root} />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board,
  accounts: state.eth.accounts,
  notLoggedIntoMetamask: state.eth.notLoggedIntoMetamask
});

export default withRouter(
  connect(
    mapStateToProps,
    {
      loadBoard,
      loadBoardExpanded,
      loadAccounts,
      loadPostsLength
    }
  )(withStyles(styles)(App))
);
