import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import UrlInput from "../components/UrlInput";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto",
    padding: theme.spacing.unit
  },
  table: {
    minWidth: 700
  }
});

class BoardInfo extends Component {
  render() {
    let { classes, board, account } = this.props;
    return (
      <Grid container>
        {account &&
          board.owner === account && (
            <Paper className={classes.root}>
              <UrlInput />
            </Paper>
          )}
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Property</TableCell>
                <TableCell>Value</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {Object.keys(this.props.board).map((boardAttr, index) => {
                return (
                  <TableRow key={index}>
                    <TableCell>{boardAttr}</TableCell>
                    <TableCell>
                      {this.props.board[boardAttr].toString()}
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Paper>
      </Grid>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board,
  account: state.eth.accounts[0]
});

export default withRouter(
  connect(mapStateToProps)(withStyles(styles)(BoardInfo))
);
