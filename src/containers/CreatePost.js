import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PostForm from "../components/PostForm";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
  paper: {
    marginTop: theme.spacing.unit * 3,
    padding: theme.spacing.unit
  },
  costExplainer: {
    marginTop: theme.spacing.unit * 3,
    textAlign: "center",
    padding: theme.spacing.unit * 3
  }
});

class BoardInfo extends Component {
  render() {
    return (
      <Grid container alignItems="center" justify="center">
        <Grid item xs={12} sm={6}>
          {this.props.board.fee &&
            parseInt(this.props.board.fee, 10) > 0 && (
              <Paper className={this.props.classes.costExplainer}>
                <Typography color="secondary">
                  THE COST IS .25 ETH PER JOB POST
                </Typography>
              </Paper>
            )}

          <Paper className={this.props.classes.paper}>
            <PostForm />
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board
});

export default withRouter(
  connect(mapStateToProps)(withStyles(styles)(BoardInfo))
);
