import React from "react";
import { Provider } from "react-redux";
import { Route } from "react-router-dom";
import App from "./App";
import PostsPage from "./PostsPage";
import BoardInfo from "./BoardInfo";
import CreatePost from "./CreatePost";
import { withStyles } from "@material-ui/core/styles";
import EthSnackbar from "../components/EthSnackbar";

const styles = {};

const Root = ({ store }) => {
  return (
    <Provider store={store}>
      <div>
        <Route path="/" component={App} />
        <Route exact path="/" component={PostsPage} />
        <Route exact path="/boardinfo" component={BoardInfo} />
        <Route exact path="/createpost" component={CreatePost} />
        <EthSnackbar />
      </div>
    </Provider>
  );
};

export default withStyles(styles)(Root);
