import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { loadPosts } from "../actions/post-actions";
import { connect } from "react-redux";
import Post from "../components/Post";
import web3 from "../ethereum/web3";
import Jumbo from "../components/Jumbo";
import UrlInput from "../components/UrlInput";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import grey from "@material-ui/core/colors/grey";

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: "center",
    color: theme.palette.text.primary
  },
  noUrlExplainerContainer: {
    marginBottom: theme.spacing.unit * 2,
    backgroundColor: grey[600]
  },
  link: {
    color: "rgb(225, 0, 80)",
    textDecoration: "none",
    "&:hover": {
      textDecoration: "underline"
    }
  },
  hr: {
    width: "50%",
    backgroundColor: "#fff"
  },
  divider: {
    width: "50%",
    margin: "12px auto"
  }
});

class Posts extends Component {
  componentDidMount() {
    if (!web3) {
      return;
    }
    this.props.loadPosts();
  }

  // static getDerivedStateFromProps(props, state) {
  //   console.log("getDerivedStateFromProps", props, state);
  // }

  render() {
    const { classes, board, accounts, setUrlRequest, posts } = this.props;

    let showBoardUrlInput =
      board.nameStr &&
      !board.urlStr &&
      board.owner === accounts[0] &&
      !setUrlRequest;

    return (
      <div>
        {showBoardUrlInput && (
          <Grid>
            <Paper
              className={classes.paper + " " + classes.noUrlExplainerContainer}
            >
              <Typography>
                Your board will not be visible on the{" "}
                <a href="https://www.decentralizedposts.com">
                  decentralizedposts.com
                </a>{" "}
                homepage until you set a URL
              </Typography>
              <UrlInput />
            </Paper>
          </Grid>
        )}
        <div className="posts">
          {accounts[0] && (
            <Grid container spacing={24}>
              <Jumbo />

              {posts.map(post => {
                return <Post post={post} key={post.index} />;
              })}
            </Grid>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board,
  posts: state.posts,
  accounts: state.eth.accounts,
  setUrlRequest: state.eth.setUrlRequest
});

export default withRouter(
  connect(
    mapStateToProps,
    {
      loadPosts
    }
  )(withStyles(styles)(Posts))
);
