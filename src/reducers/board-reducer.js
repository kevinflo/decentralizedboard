import * as types from "../actions/action-types";

export default (state = {}, action) => {
  switch (action.type) {
    case types.BOARD_REQUEST:
      return { fetching: true };
    case types.BOARD_SUCCESS:
      return { ...action.boardData };
    case types.BOARD_POSTS_LENGTH_REQUEST:
      return { ...state, postsLength: 0 };
    case types.BOARD_POSTS_LENGTH_SUCCESS:
      return { ...state, postsLength: action.postsLength };
    case types.BOARD_SET_URL_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
