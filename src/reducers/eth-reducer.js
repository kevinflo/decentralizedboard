import * as types from "../actions/action-types";

export default (
  state = { accounts: [], notLoggedIntoMetamask: false, setUrlRequest: false },
  action
) => {
  switch (action.type) {
    case types.ACCOUNTS_SUCCESS:
      return {
        ...state,
        accounts: action.accounts,
        notLoggedIntoMetamask: false
      };
    case types.ACCOUNTS_FAILURE:
      return { ...state, notLoggedIntoMetamask: true };
    case types.BOARD_SET_URL_REQUEST:
      return { ...state, setUrlRequest: true };
    case types.BOARD_SET_URL_SUCCESS:
      return { ...state, setUrlRequest: false };
    case types.BOARD_SET_URL_FAILURE:
      return { ...state, setUrlRequest: false };
    default:
      return state;
  }
};
