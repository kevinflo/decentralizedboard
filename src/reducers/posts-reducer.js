import * as types from "../actions/action-types";

export default (state = [], action) => {
  switch (action.type) {
    case types.GET_POSTS_SUCCESS:
      return [...action.posts];
    case types.SET_POST_VISIBILITY_REQUEST:
      return [...state];
    case types.SET_POST_VISIBILITY_SUCCESS:
      return [...state];
    case types.SET_POST_VISIBILITY_FAILURE:
      return [...state];
    default:
      return state;
  }
};
