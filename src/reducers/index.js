import people from "./people-reducer.js";
import eth from "./eth-reducer.js";
import board from "./board-reducer.js";
import posts from "./posts-reducer.js";
import pendingPosts from "./pending-posts-reducer.js";

import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
  people,
  eth,
  board,
  posts,
  pendingPosts,
  form: formReducer
});

export default rootReducer;
