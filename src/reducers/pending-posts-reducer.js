import * as types from "../actions/action-types";

export default (state = {}, action) => {
  let nextState;
  switch (action.type) {
    case types.POST_REQUEST:
      console.log("postRequest2", action);
      return { ...state, [action.payload.pendingId]: "REQUEST" };
    case types.POST_SUCCESS:
      nextState = { ...state };
      delete nextState[action.payload.pendingId];
      return nextState;
    case types.POST_FAILURE:
      nextState = { ...state };
      delete nextState[action.payload.pendingId];
      return nextState;
    default:
      return state;
  }
};
