import React from "react";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: "center",
    color: theme.palette.text.primary
  },
  hr: {
    width: "50%",
    backgroundColor: "#fff"
  },
  divider: {
    width: "50%",
    margin: "12px auto"
  }
});

function Jumbo(props) {
  const { classes } = props;
  return (
    <Grid item xs={12}>
      <Paper className={classes.paper}>
        <Typography variant="headline">{props.board.nameStr}</Typography>
        <Divider className={classes.divider} />
        <Typography variant="subheading">
          Proudly part of the{" "}
          <a
            className={classes.link}
            href="https://www.decentralizedposts.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Federated Network of Decentralized Posts
          </a>
        </Typography>
      </Paper>
    </Grid>
  );
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board,
  accounts: state.eth.accounts
});

export default connect(mapStateToProps)(withStyles(styles)(Jumbo));
