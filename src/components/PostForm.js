import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { isURL } from "validator";
import { createPost } from "../actions/post-actions";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto",
    padding: theme.spacing.unit
  },
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  menu: {
    width: 200
  }
});

const renderInput = ({
  input,
  label,
  meta: { touched, error, pristine },
  meta,
  check,
  ...custom
}) => {
  if (check) {
    return (
      <FormControlLabel
        control={
          <Checkbox
            checked={input.value ? true : false}
            onChange={input.onChange}
          />
        }
        label={label}
      />
    );
  } else {
    return (
      <div>
        <TextField
          margin="normal"
          fullWidth
          error={touched && !!error && !pristine}
          label={label}
          {...input}
          {...custom}
        />
        {touched && !!error && <Typography color="error">{error}</Typography>}
      </div>
    );
  }
};

const validate = values => {
  const errors = {};
  const requiredFields = ["createPostTitle", "createPostBody"];
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = "Required";
    }
  });

  if (values.createPostLink && !isURL(values.createPostLink)) {
    errors.createPostLink = "Provided URL must be valid";
  }

  if (values.createPostTitle && values.createPostTitle.length > 32) {
    errors.createPostTitle = `Provided title must be under 32 characters long (currently ${
      values.createPostTitle.length
    })`;
  }

  if (values.createPostLink && values.createPostLink.length > 32) {
    errors.createPostLink = `Provided URL must be under 32 characters long (currently ${
      values.createPostLink.length
    })`;
  }

  return errors;
};

const actionizeCreatePost = (createPost, values) => {
  createPost({
    title: values.createPostTitle,
    body: values.createPostBody,
    link: values.createPostLink,
    boardTip: values.createPostBoardTip,
    taxSubmitted: values.createPostTaxSubmitted
  });
};

class PostForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { board } = this.props.board;
    if (this.props.account) {
      const {
        handleSubmit,
        createPost,
        pristine,
        submitting,
        board
      } = this.props;
      console.log("daboard", board);
      return (
        <form
          onSubmit={handleSubmit(actionizeCreatePost.bind(null, createPost))}
        >
          <Field
            component={renderInput}
            type="text"
            name="createPostTitle"
            id="createPostTitle"
            placeholder="Job Title"
            label="Post Title"
          />
          <Field
            component={renderInput}
            type="textarea"
            name="createPostBody"
            id="createPostBody"
            label="Post Body"
            placeholder="Post Body"
            multiline
          />
          <Field
            component={renderInput}
            type="text"
            name="createPostLink"
            id="createPostLink"
            placeholder="https://postlink.com"
            label="Link"
          />

          {board.allowBoardTipping && (
            <Field
              component={renderInput}
              type="number"
              name="createPostBoardTip"
              id="createPostBoardTip"
              placeholder="0.001"
              label="Board Tip (ETH)"
            />
          )}

          {board.allowTaxation && (
            <Field
              component={renderInput}
              type="checkbox"
              name="createPostTaxSubmitted"
              id="createPostTaxSubmitted"
              check
              label="Pay platform tax (voluntary)"
            />
          )}

          <br />
          <Button
            disabled={pristine || submitting}
            variant="raised"
            color="primary"
            type="submit"
          >
            Submit
          </Button>
        </form>
      );
    } else {
      return <div>must sign in</div>;
    }
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board,
  account: state.eth.accounts[0]
});

export default reduxForm({
  form: "createPost",
  validate
})(
  withRouter(
    connect(
      mapStateToProps,
      { createPost }
    )(withStyles(styles)(PostForm))
  )
);
