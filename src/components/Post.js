import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { toggleVisibility } from "../actions/post-actions";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Divider } from "@material-ui/core";

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: "center",
    color: theme.palette.text.primary,
    borderRaidus: 8
  },
  hr: {
    width: "50%",
    backgroundColor: "#fff"
  },
  divider: {
    width: "50%",
    margin: "12px auto"
  }
});

class Post extends Component {
  toggleVisibility(e) {
    e.preventDefault();

    this.props.toggleVisibility(this.props.post.index);
  }

  render() {
    let { post, classes } = this.props;
    let currentUserPosted = post.poster === this.props.account;
    let visible = currentUserPosted || post.visible;

    return (
      <Grid item xs={12}>
        <Paper hidden={!visible} className={classes.paper}>
          <Typography variant="headline">{post.titleStr}</Typography>
          <Typography variant="body1">{post.body}</Typography>
          <p />
          {post.linkStr ? (
            <Typography variant="body1">
              Link:{" "}
              <a href={post.linkStr} target="_blank">
                {post.linkStr}
              </a>
            </Typography>
          ) : (
            <Typography variant="body2">No link provided</Typography>
          )}
          {currentUserPosted && (
            <React.Fragment>
              <Divider className={classes.divider} />
              <Typography variant="body2">You created this post.</Typography>
              <Button
                onClick={this.toggleVisibility.bind(this)}
                variant="raised"
              >
                {post.visible ? "Hide Post" : "Unhide Post"}
              </Button>
            </React.Fragment>
          )}
        </Paper>
      </Grid>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board,
  posts: state.posts,
  account: state.eth.accounts[0]
});

export default withRouter(
  connect(mapStateToProps, {
    toggleVisibility
  })(withStyles(styles)(Post))
);
