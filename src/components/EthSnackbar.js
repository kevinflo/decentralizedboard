import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Snackbar from "@material-ui/core/Snackbar";

const styles = theme => ({
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4
  }
});

class EthSnackbar extends React.Component {
  constructSnackMessage(setUrlRequest, pendingPosts) {
    let snackMessage = "";

    if (setUrlRequest) {
      snackMessage = "Url change will take a few minutes once submitted...";
    }
    let pendingPostsCount = Object.keys(pendingPosts).length;
    if (pendingPostsCount === 1) {
      snackMessage = "1 post awaiting send or posting to the blockchain";
    } else if (pendingPostsCount) {
      snackMessage = `${pendingPostsCount} posts awaiting send or posting to the blockchain`;
    }
    return snackMessage;
  }

  render() {
    const { setUrlRequest, pendingPosts } = this.props;

    let snackMessage = this.constructSnackMessage(setUrlRequest, pendingPosts);

    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={setUrlRequest || !!Object.keys(pendingPosts).length}
          autoHideDuration={6000}
          onClose={this.handleClose}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">{snackMessage}</span>}
          action={[]}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board,
  account: state.eth.accounts[0],
  setUrlRequest: state.eth.setUrlRequest,
  pendingPosts: state.pendingPosts
});

export default withRouter(
  connect(mapStateToProps)(withStyles(styles)(EthSnackbar))
);
