import React from "react";

export default () => {
  return (
    <a
      target="_blank"
      rel="noopener noreferrer"
      className="kevinflo-by"
      href="https://www.kevinflo.com"
    >
      <img
        src="https://kevinflo.com/kevinflo.jpg"
        alt="kevinflo profile avatar - what a beautiful man"
      />
      <p>by kevinflo</p>
    </a>
  );
};
