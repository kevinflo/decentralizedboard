import React from "react";
import { LinkContainer } from "react-router-bootstrap";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const styles = {
  root: {
    flexGrow: 1
  },
  flex: {
    flex: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  appBar: {
    marginBottom: 24
  }
};

class MainNavbar extends React.Component {
  state = {
    isOpen: false
  };

  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    const { classes, accounts } = this.props;
    if (accounts.length) {
      return (
        <div className={classes.root}>
          <AppBar className={classes.appBar} position="static" color="primary">
            <Toolbar>
              <Typography
                variant="title"
                color="inherit"
                className={classes.flex}
              >
                {this.props.board.nameStr}
              </Typography>
              <LinkContainer to="/">
                <Button color="inherit">Home</Button>
              </LinkContainer>
              <LinkContainer to="/boardinfo">
                <Button color="inherit">Board Info</Button>
              </LinkContainer>
              <LinkContainer to="/createpost">
                <Button
                  variant="raised"
                  color="secondary"
                  className={classes.button}
                >
                  New Post
                </Button>
              </LinkContainer>
            </Toolbar>
          </AppBar>
        </div>
      );
    }

    return null;
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board,
  accounts: state.eth.accounts
});

export default withRouter(
  connect(mapStateToProps)(withStyles(styles)(MainNavbar))
);
