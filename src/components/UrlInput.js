import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { isURL } from "validator";
import { boardSetUrl } from "../actions/board-actions";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  menu: {
    width: 200
  }
});

const renderInput = ({
  input,
  label,
  meta: { touched, error, pristine },
  ...custom
}) => {
  return (
    <div>
      <TextField
        margin="normal"
        error={touched && !!error && !pristine}
        label={label}
        {...input}
        {...custom}
      />
      {touched && !!error && <Typography color="error">{error}</Typography>}
    </div>
  );
};

const validate = values => {
  const errors = {};
  const requiredFields = ["boardUrl"];
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = "Required";
    }
  });

  if (values.boardUrl && !isURL(values.boardUrl)) {
    errors.boardUrl = "Provided URL must be valid";
  }

  if (values.boardUrl && values.boardUrl.length > 32) {
    errors.boardUrl = `Provided URL must be under 32 characters long (currently ${
      values.boardUrl.length
    })`;
  }

  return errors;
};

const actionizeBoardSetUrl = (boardSetUrl, values) => {
  console.log("actionizeBoardSetUrl");
  boardSetUrl(values.boardUrl);
};

class UrlInput extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      handleSubmit,
      pristine,
      submitting,
      boardSetUrl,
      setUrlRequest
    } = this.props;

    return (
      <form
        onSubmit={handleSubmit(actionizeBoardSetUrl.bind(null, boardSetUrl))}
      >
        <Field
          component={renderInput}
          type="text"
          name="boardUrl"
          id="boardUrl"
          placeholder="yourawesomeboard.com"
          label="Update board URL"
          fullWidth
        />
        <Button
          variant="raised"
          color="secondary"
          type="submit"
          disabled={pristine || submitting || setUrlRequest}
        >
          Submit
        </Button>
      </form>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  board: state.board,
  setUrlRequest: state.eth.setUrlRequest
});

export default reduxForm({
  form: "urlInput",
  validate
})(
  withRouter(
    connect(mapStateToProps, { boardSetUrl })(withStyles(styles)(UrlInput))
  )
);
