import React from "react";
import { render } from "react-dom";
// import registerServiceWorker from "./registerServiceWorker";
import configureStore from "./store/configureStore";
import { BrowserRouter as Router } from "react-router-dom";
import Root from "./containers/Root";
import "./containers/App.css";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import blue from "@material-ui/core/colors/blue";
import grey from "@material-ui/core/colors/grey";
import ReactGA from "react-ga";
import { GA_KEY } from "./secrets";

if (
  window &&
  window.location &&
  process &&
  process.env &&
  process.env.NODE_ENV === "production"
) {
  ReactGA.initialize(GA_KEY);
  ReactGA.pageview(window.location.pathname + window.location.search);
}

const theme = createMuiTheme({
  palette: {
    primary: blue,
    type: "dark"
  },
  root: {
    backgroundColor: grey[900]
  }
});

const store = configureStore();
render(
  <Router>
    <MuiThemeProvider theme={theme}>
      <Root store={store} />
    </MuiThemeProvider>
  </Router>,
  document.getElementById("root")
);
// registerServiceWorker();
