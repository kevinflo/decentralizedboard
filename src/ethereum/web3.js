import Web3 from "web3";
import { INFURA_RINKEBY } from "../secrets";

let web3;
if (typeof window !== "undefined" && typeof window.web3 !== "undefined") {
  web3 = new Web3(window.web3.currentProvider);
  web3.isServer = false;
} else {
  const provider = new Web3.providers.HttpProvider(INFURA_RINKEBY);
  web3 = new Web3(provider);
  web3.isServer = true;
}

export default web3;
