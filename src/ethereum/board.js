import web3 from "./web3";
import Board from "./build/Board.json";
import { BOARD4 } from "../addresses";
let instance;
if (web3) {
  instance = new web3.eth.Contract(JSON.parse(Board.interface), BOARD4);
}
export default instance;
