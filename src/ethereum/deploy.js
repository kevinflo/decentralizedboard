const HDWalletProvider = require("truffle-hdwallet-provider");
const Web3 = require("web3");
const compiledDposts = require("./build/Dposts.json");
const WALLET_KEY = require("../wallet-key.json").WALLET_KEY;
const INFURA_RINKEBY = require("../secrets.json").INFURA.RINKEBY;
const INFURA_MAINNET = require('../secrets.json').INFURA2.MAINNET;

const provider = new HDWalletProvider(WALLET_KEY, INFURA_MAINNET);
const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();
  let gasCost = await new web3.eth.Contract(
    JSON.parse(compiledDposts.interface)
  )
    .deploy({ data: compiledDposts.bytecode })
    .estimateGas({ from: accounts[0] });

  const result = await new web3.eth.Contract(
    JSON.parse(compiledDposts.interface)
  )
    .deploy({ data: compiledDposts.bytecode })
    .send({ gas: gasCost, from: accounts[0] });
};
deploy();
