const path = require('path');
const solc = require('solc');
const fs = require('fs-extra');

const buildPath = path.resolve(__dirname, 'build');

fs.removeSync(buildPath);

const dpostsPath = path.resolve(__dirname, 'contracts', 'Dposts.sol');
const source = fs.readFileSync(dpostsPath, 'utf8');
const output = solc.compile(source, 1).contracts;

fs.ensureDirSync(buildPath);

for (let contract in output) {
  fs.outputJsonSync(
    path.resolve(buildPath, contract.replace(':', '') + '.json'),
    output[contract]
  );
}

// module.exports = {
//   Dposts: contracts[':Dposts'],
//   Board: contracts[':Board']
// };
