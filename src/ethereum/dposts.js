import web3 from "./web3";
import Dposts from "./build/Dposts.json";
import { DPOSTS_RINKEBY6 } from "../addresses";

const instance = new web3.eth.Contract(
  JSON.parse(Dposts.interface),
  DPOSTS_RINKEBY6
);

export default instance;
